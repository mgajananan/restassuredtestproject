package com.api.helpers.models;

public class UsersResponse {

    public Integer id;
    public String name;
    public String job;
    public Integer age;
    public String createdAt;

    public UsersResponse getUsersResponse() {
        return this;
    }

    public void setUsersResponse(String fName, String lName, Integer age, String createdAt){
    	this.name = fName;
        this.job = lName;
        this.age = age;
        this.createdAt = createdAt;
    }
}