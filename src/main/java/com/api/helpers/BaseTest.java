package com.api.helpers;

import java.io.IOException;
import java.util.Properties;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class BaseTest {
	public RequestSpecification REQUEST;

	public BaseTest() {
		try {
			
			Properties props = new Properties();
			props.load(getClass().getClassLoader().getResourceAsStream("config.properties"));

			// Rest Assured config
			RestAssured.baseURI = props.getProperty("api.uri");	
			// RestAssured.port = Integer.valueOf(props.getProperty("api.port"));
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		
		// basic request setting
		REQUEST = RestAssured.given().contentType(ContentType.JSON);
	}
}

