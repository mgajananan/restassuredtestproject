package com.api.specs;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.api.helpers.BaseTest;
import com.api.helpers.models.SingleUserResponse;

import io.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ListUsers extends BaseTest {
	

	@Test
	public void shouldFetchListOfAllUsers() {
		REQUEST
			.get("/users")
			.then()
				.assertThat()
				.statusCode(200)
				.contentType(ContentType.JSON)
				.statusLine(containsString("OK"))
				.log().body()
				.body("data", hasSize(3))
				.body("data[0].id", equalTo(1))
				.body("data[0].first_name", equalTo("George"))
				.body("data.findAll{it.id<2}.first_name", hasItem("George"))
				.body("data[0].last_name", not(equalTo("George")));
		
	}
	
	@Test
	public void verifySecondUser()
	{
		REQUEST
			.get("/users/2")
			.then()
			.assertThat()
			.statusCode(200)
			.statusLine(containsString("OK"))
			.log().body()
			.body("data.id", equalTo(2));
	}
	 
	 @Test
	public void shouldGetSecondUserVerifyUsingJsonSchemaValidator() {
		
		String json = REQUEST.get("/users/2").body().asString();
		
		REQUEST.get("/users/2")
			.then()
				.statusCode(200)
				.time(lessThan(2L), TimeUnit.SECONDS)
				.log().all();
			
		assertThat(json, matchesJsonSchemaInClasspath("user2.json"));
	}
	 
	 
@Test
public void firstuserverification()
{
	SingleUserResponse user1 = REQUEST
	.get("/users/1")
	.as(SingleUserResponse.class);
	
	Assert.assertEquals(user1.getData().getFirstName(),"George");
}
	 
	 
	 
	@Test
	public void shouldGetSecondUserAndVerifyFirstNameAndId() {
		SingleUserResponse usersResponse = REQUEST
				.get("/users/2")
				.as(SingleUserResponse.class);
		Assert.assertEquals(Integer.valueOf(2), usersResponse.getData().getId());
		Assert.assertEquals("Janet", usersResponse.getData().getFirstName());
		
		System.out.println( usersResponse.getData().getId());
		System.out.println( usersResponse.getData().getFirstName());
		System.out.println( usersResponse.getData().getLastName());
		System.out.println( usersResponse.getData().getEmail());
		System.out.println( usersResponse.getData().getAvatar());

	}
	

	
	@Test
	public void shouldGetUserNotFound() {
		REQUEST.get("/users/777")
			.then()
				.statusCode(404)
				.statusLine("HTTP/1.1 404 Not Found");

	}
	
	@Test
	public void shouldFetchListOfAllUsersGreaterThanId2() {
		REQUEST.get("/users")
			.then()
				.statusCode(200)
			.and()
				.body("data.findAll{it.id>2}.first_name", hasItem("Emma"));
	}
	
	@Test
	public void shouldCheckUsersCount() {
		REQUEST.get("/users")
			.then()
				.statusCode(200)
			.and()
				.body("data", hasSize(3));
	}

	


}
