package com.api.specs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//JUnit Suite Test
@RunWith(Suite.class)

@Suite.SuiteClasses({ 
	ListUsers.class , ListResource.class, UserBusinessFlow.class
})

public class TestSuite {
}

  