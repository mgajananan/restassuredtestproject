package com.api.specs;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;

import org.junit.Test;

import com.api.helpers.BaseTest;
import com.api.helpers.models.CreateUserRequest;
import com.api.helpers.models.UsersResponse;

import io.restassured.authentication.FormAuthConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class UserBusinessFlow extends BaseTest {

	
	private static int id;
	HashMap<String, String> jsonAsMap = new HashMap<String,String>();
	
	
//	@Test
	public void basicAuthorization() {
		REQUEST.auth()
		  .basic("user1", "user1Pass")
		  .when()
		  .get("http://localhost:8080/spring-security-rest-basic-auth/api/foos/1")
		  .then()
		  .assertThat()
		  .statusCode(200);
	}
	
	public class OAuth2Test {
	    private String resourceOwnerLogin(String tokenUri, String clientId, String clientSecret, String username, String password, String scope) {
	       
	       Response response = REQUEST.auth().preemptive().basic(clientId, clientSecret)   
	                        .formParam("grant_type", "password")
	                        .formParam("username", username)
	                        .formParam("password", password)
	                        .formParam("scope", scope)
	                        .when()
	                        .post(tokenUri);
	       
	        String accessToken = response.path("access_token").toString();
	        String tokenType = response.path("token_type").toString();
	        
	        String dateHeader  = response.getHeader("Expires");
	        String respCookie = response.getCookie("auth_cookie");
	        return accessToken;
	    }
	}
	
	//@Test
	public void formAuthorization() {
		REQUEST.auth()
		  .form(
				    "user1",
				    "user1Pass",
				    new FormAuthConfig("/perform_login", "username", "password"))
		  .when()
		  .get("http://localhost:8080/spring-security-rest-basic-auth/api/foos/1")
		  .then()
		  .assertThat()
		  .statusCode(200);
	}
	
	@Test
	public void createUser() {
		
		jsonAsMap.put("name", "John");
		jsonAsMap.put("job", "qa");
		
		REQUEST.contentType(ContentType.JSON)
				.body(jsonAsMap)
			.post("/users")
			.then()
				.assertThat()
				.statusCode(201)
				.contentType(ContentType.JSON)
				.statusLine(containsString("Created"))
				.log().body();
			
	}
	
	@Test
	public void createUserWithPojo() {
		
		CreateUserRequest request = new CreateUserRequest();
		request.setName("Jane");
		request.setJob("UX");
		
		UsersResponse usersResponse = REQUEST.contentType(ContentType.JSON)
				.body(request)
			.post("/users")
			.as(UsersResponse.class);
		
		id = usersResponse.id;
		System.out.println(usersResponse.id);
		System.out.println(usersResponse.name);
		System.out.println(usersResponse.job);
		System.out.println(usersResponse.createdAt);
			
	}
	
	
	//@Test
	public void updateUser() {
		jsonAsMap.put("job", "UI");
		
		REQUEST.body(jsonAsMap).put("/users/"+id)
			.then()
				.statusCode(200)
			.and()
				.body("data[0].job", equalTo("UI"))
				.log().body();

	}

	//@Test
	public void deleteUser() {
		REQUEST.delete("/users/"+id)
			.then()
				.statusCode(204);

	}
	
}
