package com.api.specs;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

import java.util.HashMap;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.api.helpers.BaseTest;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ListResource extends BaseTest {

	private static HashMap<String, String> jsonAsMap = new HashMap<String, String>();

	@Test
	public void login() {

		jsonAsMap.put("email", "eve.holt@reqres.in");
		jsonAsMap.put("password", "cityslicka");

		Response response = REQUEST
				// .auth()
				// .basic("eve.holt@reqres.in", "cityslicka")
				// .when()
				.contentType(ContentType.JSON).body(jsonAsMap).post("/login").then().log().ifError().statusCode(200)
				.body("$", hasKey("token")).extract().response();

		String auth_token = response.path("token").toString();
		System.out.println("token :: " + auth_token);

	}

	@Test
	public void shouldFetchListOfAllUsers() {
		REQUEST.get("/unknown").then().statusCode(200);
	}

	@Test
	public void shouldGetSecondUserAndVerifyFirstNameAndId() {
		REQUEST.get("/unknown/2").then().statusCode(200).and().body("data.id", equalTo(2)).body("data.name",
				equalTo("fuchsia rose"));

	}

	@Test
	public void shouldGetSecondUserVerifyUsingJsonSchemaValidator() {

		String json = REQUEST.get("/unknown/2").body().asString();
		REQUEST.get("/unknown/2").then().statusCode(200);

		assertThat(json, matchesJsonSchemaInClasspath("resource2.json"));

	}

	@Test
	public void shouldGetUserNotFound() {
		REQUEST.get("/unknown/777").then().statusCode(404);

	}

}
